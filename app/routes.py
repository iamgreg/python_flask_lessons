# -*- coding: utf-8 -*-
from app import app, db
from flask import render_template, url_for, request
from app.models import User, Post

@app.route('/')
@app.route('/index')
def index():
    #в данный момент приложение ещё не имеет концепции пользователей,
    #а мы хотим, чтобы наше приложение привествовало пользователей
    #в таких ситуациях используются вымышленные (mock) пользователи.

    #заведём словарь для задания пользователя
    user = {'username': 'инкогнито'}
    #справка о словарях в python
    #https://tproger.ru/explain/python-dictionaries/

    #заведём список словарей - для задания постов о других пользователях:
    #Списки в Python - упорядоченные изменяемые коллекции объектов произвольных типов
    #(почти как массив). https://tproger.ru/translations/python-data-types/
    posts = [
        {
            'author': {'username': 'Alexander'},
            'body': 'Коллеги, с днём 8-го Марта!'
        },
        {
            'author': {'username': 'Denis'},
            'body': 'С чудесным праздником весны!'
        },
        {
            'author': {'username': 'Kateryna'},
            'body': 'Желаю замечательного настроения и много-много улыбок!'
        }
    ]
    #return "Hello, World!"
    return render_template("index.html", title='Главная страницы', user=user, posts=posts)


@app.route('/hello', methods=['GET', 'POST'])
def hello():
    user = request.form['user_name']
    print(user,type(user))
    posts = [
        {
            'author': {'username': 'Kateryna'},
            'body': 'Желаю замечательного настроения и много-много улыбок!'
        }
    ]
    return render_template("hello.html", title='Страница приветствия', user=user, posts=posts)


@app.route('/blank', methods=['GET', 'POST'])
@app.route('/blank')
def blank():
    if request.method == 'GET':
        return '<html><h2>it is blank!</h2></html>'
    else:
        output = list(range(10))
        if request.form['author_name'][-1] not in ['у', 'е', 'ы', 'а', 'о', 'э', 'ё', 'я', 'и', 'ю']:
            output = reversed(output)


        output = ", ".join(map(lambda x: str(x), output))
        return render_template("blank.html", title='Страница автора', range=output)

@app.route('/posts')
def posts():
    user_one = User(username='IVAN2', email='admin@google.com')

#    db.session.add(user_one)
#    db.session.commit()

    all_users = User.query.all()
    all_posts = Post.query.all()
    simple_dict={
        'body':'привет',
        'user_name':'Ivan'
    }
    list_od_posts=[]
    for post in all_posts:
        user_for_post = User.query.filter_by(id=post.user_id).first_or_404()
        print(user_for_post)
        list_od_posts.append(
            {
                'body': post.body,
                'user_name': user_for_post.username
            }
        )
    print(list_od_posts)

    return render_template("posts2.html", all_posts=list_od_posts)
