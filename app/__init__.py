from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy #new
from flask_migrate import Migrate #new
import flask_admin as admin #добавляем админа
from flask_admin.contrib.sqla import ModelView


app = Flask(__name__)
app.config.from_object(Config)  #new
db = SQLAlchemy(app)  #new
migrate = Migrate(app, db)  #new

from app import routes, models #new


class FlaskyAdminIndexView(admin.AdminIndexView):
    @admin.expose('/')
    def index(self):
        pass
        return super(FlaskyAdminIndexView, self).index()



admin = admin.Admin(index_view=FlaskyAdminIndexView(),name="MIPT FLASK", template_mode='bootstrap3')
admin.add_view(ModelView(models.User, db.session))
admin.add_view(ModelView(models.Post, db.session))
admin.init_app(app)
